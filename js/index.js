jQuery(document).ready(function () {
    jQuery('.willscot-list-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        arrows: true,
        infinite: true,
        //asNavFor: ".willscot-list-modal-slider",
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

    var clickedSlick;

    jQuery('.willscot-list-slider-slide').each(function () {

        jQuery(this).click(function (e) {
            e.preventDefault();
            var test = jQuery(this).closest('.slick-active').attr('data-slick-index');
            clickedSlick = test;
        });

    })


    jQuery('.willscot-list-slider .willscot-list-modal-slider').removeClass('slick-active');

    //set active class to first thumbnail slides
    jQuery('.willscot-list-slider .willscot-list-modal-slider').eq(0).addClass('slick-active');



    jQuery('#modalId').on('shown.bs.modal', function () {
        jQuery('.willscot-list-modal-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: true,
            infinite: true,
            asNavFor: ".willscot-list-slider",
        });

        jQuery('.willscot-list-modal-slider').slick('slickGoTo', clickedSlick);
    });
    jQuery("#modalId").on('hidden.bs.modal', function (e) {
        jQuery('.willscot-list-modal-slider').slick('unslick')
    });
});
